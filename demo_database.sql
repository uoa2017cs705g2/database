-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 05, 2017 at 05:35 PM
-- Server version: 5.6.35
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `statusUpdater`
--

-- --------------------------------------------------------

--
-- Table structure for table `contactgroup`
--

CREATE TABLE `contactgroup` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `owner` int(11) NOT NULL,
  `permission_set` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contactgroup`
--

INSERT INTO `contactgroup` (`id`, `name`, `owner`, `permission_set`) VALUES
(1, 'Work', 3, 3),
(2, 'Family', 3, 2),
(3, '3ContactGroup5TestingOverrides', 3, 6);

-- --------------------------------------------------------

--
-- Stand-in structure for view `contactgroup_users`
-- (See below for the actual view)
--
CREATE TABLE `contactgroup_users` (
`user_id` int(11)
,`friend_id` int(11)
,`friend_firstname` varchar(45)
,`friend_lastname` varchar(45)
,`contactgroup_id` int(11)
,`contactgroup_name` varchar(45)
,`permissionset_id` int(11)
,`permissionset_name` varchar(45)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `friend_users`
-- (See below for the actual view)
--
CREATE TABLE `friend_users` (
`user_id` int(11)
,`friend_id` int(11)
,`friend_firstname` varchar(45)
,`friend_lastname` varchar(45)
,`friend_username` varchar(45)
,`permissionset_id` int(11)
,`permissionset_name` varchar(45)
);

-- --------------------------------------------------------

--
-- Table structure for table `guild`
--

CREATE TABLE `guild` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `creator` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `guild`
--

INSERT INTO `guild` (`id`, `name`, `creator`) VALUES
(1, 'StatusGauge Development Team', 3);

-- --------------------------------------------------------

--
-- Stand-in structure for view `guild_users`
-- (See below for the actual view)
--
CREATE TABLE `guild_users` (
`user_id` int(11)
,`guild_id` int(11)
,`guildname` varchar(45)
,`permissionset_id` int(11)
,`permissionset_name` varchar(45)
);

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`id`, `name`, `description`) VALUES
(1, 'PHONE_STATE', 'Shows that you are in a call'),
(2, 'PHONE_TO', 'Shows who you are on the phone to'),
(10, 'WORKING_PROGRAM', 'Show the project that you are working on'),
(11, 'WORKING_FILE', 'Show the file that you are working on'),
(12, 'SOCIAL_PLATFORM', 'Show the social media platform that the user is on');

-- --------------------------------------------------------

--
-- Stand-in structure for view `permissionset_permissions`
-- (See below for the actual view)
--
CREATE TABLE `permissionset_permissions` (
`permission_id` int(11)
,`permission_name` varchar(45)
,`permission_description` text
,`permissionset_name` varchar(45)
,`permissionset_id` int(11)
,`permission_enabled` tinyint(1)
,`permission_disabled` tinyint(1)
);

-- --------------------------------------------------------

--
-- Table structure for table `permissionset_permission_relation`
--

CREATE TABLE `permissionset_permission_relation` (
  `permission_set` int(11) NOT NULL,
  `permission` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `disabled` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissionset_permission_relation`
--

INSERT INTO `permissionset_permission_relation` (`permission_set`, `permission`, `enabled`, `disabled`) VALUES
(1, 1, 1, 0),
(1, 11, 1, 0),
(2, 1, 1, 0),
(2, 2, 1, 0),
(2, 12, 1, 0),
(3, 1, 1, 0),
(3, 10, 1, 0),
(4, 12, 1, 0),
(5, 11, 0, 1),
(5, 12, 1, 0),
(6, 11, 1, 0),
(6, 12, 0, 1),
(7, 1, 1, 0),
(7, 2, 1, 0),
(7, 10, 1, 0),
(7, 11, 0, 1),
(8, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `permission_set`
--

CREATE TABLE `permission_set` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `creator` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permission_set`
--

INSERT INTO `permission_set` (`id`, `name`, `creator`) VALUES
(1, 'statusGaugeDevelopmentTeamPermissions', 3),
(2, 'familyPermissions', 3),
(3, 'workPermissions', 3),
(4, 'friendPermissions', 3),
(5, 'myFriendUserC', 3),
(6, '3ContactGroup5', 3),
(7, '3To6Friendship', 3),
(8, '6To3Friendship', 6);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `highest` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `user`, `highest`, `timestamp`) VALUES
(1, 3, 'Online', '2017-10-04 10:58:52'),
(3, 6, 'Online', '2017-10-04 11:21:56'),
(5, 3, 'Online', '2017-10-04 12:19:43'),
(6, 3, 'Online', '2017-10-04 12:20:26'),
(7, 6, 'Online', '2017-10-04 15:31:38'),
(8, 6, 'Online', '2017-10-04 15:32:26'),
(9, 6, 'Online', '2017-10-04 15:33:35'),
(10, 6, 'Online', '2017-10-04 15:34:12'),
(11, 6, 'Online', '2017-10-04 15:35:33'),
(12, 6, 'Online', '2017-10-04 15:36:28'),
(13, 6, 'Online', '2017-10-04 15:36:55'),
(14, 6, 'Online', '2017-10-04 15:38:03'),
(15, 6, 'Online', '2017-10-04 15:38:34'),
(16, 6, 'Online', '2017-10-04 15:39:03'),
(17, 6, 'Online', '2017-10-04 15:39:31'),
(18, 6, 'Online', '2017-10-04 15:40:01'),
(19, 6, 'Online', '2017-10-04 15:40:37'),
(20, 6, 'Online', '2017-10-04 15:41:17'),
(21, 6, 'Online', '2017-10-04 15:42:32'),
(22, 6, 'Online', '2017-10-04 15:42:49'),
(23, 6, 'Online', '2017-10-04 15:42:59'),
(24, 3, 'Online', '2017-10-04 15:44:12'),
(25, 3, 'Online', '2017-10-04 15:45:33'),
(26, 3, 'Working', '2017-10-04 15:47:47'),
(27, 3, 'Working', '2017-10-04 15:48:28'),
(28, 3, 'Working', '2017-10-04 15:48:47'),
(29, 3, 'Working', '2017-10-04 15:49:20'),
(30, 3, 'Working', '2017-10-04 15:49:36'),
(31, 3, 'Working', '2017-10-04 15:50:51'),
(32, 3, 'Working', '2017-10-04 15:51:31'),
(33, 3, 'Working', '2017-10-04 15:52:28'),
(34, 3, 'Working', '2017-10-04 15:52:57'),
(35, 3, 'Working', '2017-10-05 03:26:25');

-- --------------------------------------------------------

--
-- Table structure for table `status_detail`
--

CREATE TABLE `status_detail` (
  `status_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status_detail`
--

INSERT INTO `status_detail` (`status_id`, `permission_id`, `value`) VALUES
(1, 12, 'Facebook'),
(3, 2, '0211265335'),
(3, 1, 'In Discord Call'),
(5, 12, 'Twitter'),
(6, 12, 'Twitter'),
(7, 12, 'Twitter'),
(8, 12, 'Twitter'),
(9, 12, 'Twitter'),
(10, 12, 'Twitter'),
(11, 12, 'Twitter'),
(12, 12, 'Twitter'),
(13, 12, 'Twitter'),
(14, 12, 'Twitter'),
(15, 12, 'Twitter'),
(16, 12, 'Twitter'),
(17, 12, 'Twitter'),
(18, 12, 'Twitter'),
(19, 12, 'Twitter'),
(20, 12, 'Twitter'),
(21, 12, 'Twitter'),
(22, 12, 'Twitter'),
(23, 12, 'Twitter'),
(24, 12, 'Twitter'),
(25, 12, 'Twitter'),
(26, 10, 'Google Docs'),
(27, 10, 'Google Docs'),
(28, 10, 'Google Docs'),
(29, 10, 'Google Docs'),
(30, 10, 'Google Docs'),
(31, 10, 'Google Docs'),
(32, 10, 'Google Docs'),
(33, 10, 'Google Docs'),
(34, 10, 'Google Docs'),
(35, 10, 'Google Docs');

-- --------------------------------------------------------

--
-- Stand-in structure for view `status_updates`
-- (See below for the actual view)
--
CREATE TABLE `status_updates` (
`user_id` int(11)
,`status_timestamp` timestamp
,`status_id` int(11)
,`status` text
,`detail_name` varchar(45)
,`detail_id` int(11)
,`detail_value` text
);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `gid` varchar(30) NOT NULL COMMENT 'Google ID',
  `password` varchar(45) NOT NULL,
  `salt` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `username`, `gid`, `password`, `salt`) VALUES
(1, '', '', 'subserver', '', 'password', ''),
(2, '', '', 'user', '', 'pass', ''),
(3, 'User A', '', 'usera', '', 'usera', ''),
(4, 'User B', '', 'userb', '', 'userb', ''),
(5, 'User C', '', 'userc', '', 'userc', ''),
(6, 'User D', '', 'userd', '', 'userd', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_contactgroup_relation`
--

CREATE TABLE `user_contactgroup_relation` (
  `user` int(11) NOT NULL,
  `friend` int(11) NOT NULL,
  `contactgroup` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_contactgroup_relation`
--

INSERT INTO `user_contactgroup_relation` (`user`, `friend`, `contactgroup`) VALUES
(3, 4, 1),
(3, 6, 2),
(3, 5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `user_friend_relation`
--

CREATE TABLE `user_friend_relation` (
  `user` int(11) NOT NULL,
  `friend` int(11) NOT NULL,
  `permission_set` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_friend_relation`
--

INSERT INTO `user_friend_relation` (`user`, `friend`, `permission_set`) VALUES
(3, 5, 5),
(3, 6, 7),
(6, 3, 8);

-- --------------------------------------------------------

--
-- Table structure for table `user_guild_relation`
--

CREATE TABLE `user_guild_relation` (
  `user` int(11) NOT NULL,
  `guild` int(11) NOT NULL,
  `permission_set` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_guild_relation`
--

INSERT INTO `user_guild_relation` (`user`, `guild`, `permission_set`) VALUES
(3, 1, 1),
(4, 1, 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `user_member_contactgrouprelation_permission`
-- (See below for the actual view)
--
CREATE TABLE `user_member_contactgrouprelation_permission` (
`user_id` int(11)
,`friend_id` int(11)
,`permission_id` int(11)
,`permission_name` varchar(45)
,`permission_description` text
,`permission_enabled` tinyint(1)
,`permission_disabled` tinyint(1)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `user_member_guildrelation_permission`
-- (See below for the actual view)
--
CREATE TABLE `user_member_guildrelation_permission` (
`user_id` int(11)
,`friend_id` int(11)
,`guild_id` int(11)
,`guild_name` varchar(45)
,`permission_id` int(11)
,`permission_name` varchar(45)
,`permission_description` text
,`permission_enabled` tinyint(1)
,`permission_disabled` tinyint(1)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `user_memeber_friendrelation_permission`
-- (See below for the actual view)
--
CREATE TABLE `user_memeber_friendrelation_permission` (
`user_id` int(11)
,`friend_id` int(11)
,`permission_id` int(11)
,`permission_name` varchar(45)
,`permission_description` text
,`permission_enabled` tinyint(1)
,`permission_disabled` tinyint(1)
);

-- --------------------------------------------------------

--
-- Structure for view `contactgroup_users`
--
DROP TABLE IF EXISTS `contactgroup_users`;

CREATE VIEW `contactgroup_users`  AS  select `user_user`.`id` AS `user_id`,`user_friend`.`id` AS `friend_id`,`user_friend`.`firstname` AS `friend_firstname`,`user_friend`.`lastname` AS `friend_lastname`,`contactgroup`.`id` AS `contactgroup_id`,`contactgroup`.`name` AS `contactgroup_name`,`permission_set`.`id` AS `permissionset_id`,`permission_set`.`name` AS `permissionset_name` from ((((`user_contactgroup_relation` join `user` `user_user` on((`user_contactgroup_relation`.`user` = `user_user`.`id`))) join `user` `user_friend` on((`user_contactgroup_relation`.`friend` = `user_friend`.`id`))) join `contactgroup` on((`user_contactgroup_relation`.`contactgroup` = `contactgroup`.`id`))) join `permission_set` on((`contactgroup`.`permission_set` = `permission_set`.`id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `friend_users`
--
DROP TABLE IF EXISTS `friend_users`;

CREATE VIEW `friend_users`  AS  select `user_user`.`id` AS `user_id`,`user_friend`.`id` AS `friend_id`,`user_friend`.`firstname` AS `friend_firstname`,`user_friend`.`lastname` AS `friend_lastname`,`user_friend`.`username` AS `friend_username`,`permission_set`.`id` AS `permissionset_id`,`permission_set`.`name` AS `permissionset_name` from (((`user_friend_relation` join `user` `user_user` on((`user_friend_relation`.`user` = `user_user`.`id`))) join `user` `user_friend` on((`user_friend_relation`.`friend` = `user_friend`.`id`))) join `permission_set` on((`user_friend_relation`.`permission_set` = `permission_set`.`id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `guild_users`
--
DROP TABLE IF EXISTS `guild_users`;

CREATE VIEW `guild_users`  AS  select `user`.`id` AS `user_id`,`guild`.`id` AS `guild_id`,`guild`.`name` AS `guildname`,`permission_set`.`id` AS `permissionset_id`,`permission_set`.`name` AS `permissionset_name` from (((`user_guild_relation` join `user` on((`user_guild_relation`.`user` = `user`.`id`))) join `guild` on((`user_guild_relation`.`guild` = `guild`.`id`))) join `permission_set` on((`user_guild_relation`.`permission_set` = `permission_set`.`id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `permissionset_permissions`
--
DROP TABLE IF EXISTS `permissionset_permissions`;

CREATE VIEW `permissionset_permissions`  AS  select `permission`.`id` AS `permission_id`,`permission`.`name` AS `permission_name`,`permission`.`description` AS `permission_description`,`permission_set`.`name` AS `permissionset_name`,`permission_set`.`id` AS `permissionset_id`,`permissionset_permission_relation`.`enabled` AS `permission_enabled`,`permissionset_permission_relation`.`disabled` AS `permission_disabled` from ((`permissionset_permission_relation` join `permission_set` on((`permissionset_permission_relation`.`permission_set` = `permission_set`.`id`))) join `permission` on((`permissionset_permission_relation`.`permission` = `permission`.`id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `status_updates`
--
DROP TABLE IF EXISTS `status_updates`;

CREATE VIEW `status_updates`  AS  select `user`.`id` AS `user_id`,`status`.`timestamp` AS `status_timestamp`,`status`.`id` AS `status_id`,`status`.`highest` AS `status`,`permission`.`name` AS `detail_name`,`permission`.`id` AS `detail_id`,`status_detail`.`value` AS `detail_value` from (((`status` left join `status_detail` on((`status_detail`.`status_id` = `status`.`id`))) left join `permission` on((`status_detail`.`permission_id` = `permission`.`id`))) join `user` on((`user`.`id` = `status`.`user`))) ;

-- --------------------------------------------------------

--
-- Structure for view `user_member_contactgrouprelation_permission`
--
DROP TABLE IF EXISTS `user_member_contactgrouprelation_permission`;

CREATE VIEW `user_member_contactgrouprelation_permission`  AS  select `user_user`.`id` AS `user_id`,`user_friend`.`id` AS `friend_id`,`permission`.`id` AS `permission_id`,`permission`.`name` AS `permission_name`,`permission`.`description` AS `permission_description`,`permissionset_permission_relation`.`enabled` AS `permission_enabled`,`permissionset_permission_relation`.`disabled` AS `permission_disabled` from ((((((`user_contactgroup_relation` join `user` `user_user` on((`user_contactgroup_relation`.`user` = `user_user`.`id`))) join `user` `user_friend` on((`user_contactgroup_relation`.`friend` = `user_friend`.`id`))) join `contactgroup` on((`user_contactgroup_relation`.`contactgroup` = `contactgroup`.`id`))) join `permission_set` on((`contactgroup`.`permission_set` = `permission_set`.`id`))) join `permissionset_permission_relation` on((`permission_set`.`id` = `permissionset_permission_relation`.`permission_set`))) join `permission` on((`permissionset_permission_relation`.`permission` = `permission`.`id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `user_member_guildrelation_permission`
--
DROP TABLE IF EXISTS `user_member_guildrelation_permission`;

CREATE VIEW `user_member_guildrelation_permission`  AS  select `user_user`.`id` AS `user_id`,`user_friend`.`id` AS `friend_id`,`guild`.`id` AS `guild_id`,`guild`.`name` AS `guild_name`,`permission`.`id` AS `permission_id`,`permission`.`name` AS `permission_name`,`permission`.`description` AS `permission_description`,`permissionset_permission_relation`.`enabled` AS `permission_enabled`,`permissionset_permission_relation`.`disabled` AS `permission_disabled` from (((((((`user_guild_relation` `user_user_guild_relation` join `user` `user_user` on((`user_user_guild_relation`.`user` = `user_user`.`id`))) join `guild` on((`user_user_guild_relation`.`guild` = `guild`.`id`))) join `user_guild_relation` `friend_guild_relation` on((`friend_guild_relation`.`guild` = `guild`.`id`))) join `user` `user_friend` on((`friend_guild_relation`.`user` = `user_friend`.`id`))) join `permission_set` on((`user_user_guild_relation`.`permission_set` = `permission_set`.`id`))) join `permissionset_permission_relation` on((`permission_set`.`id` = `permissionset_permission_relation`.`permission_set`))) join `permission` on((`permissionset_permission_relation`.`permission` = `permission`.`id`))) where (`user_user`.`id` <> `user_friend`.`id`) ;

-- --------------------------------------------------------

--
-- Structure for view `user_memeber_friendrelation_permission`
--
DROP TABLE IF EXISTS `user_memeber_friendrelation_permission`;

CREATE VIEW `user_memeber_friendrelation_permission`  AS  select `user_user`.`id` AS `user_id`,`user_friend`.`id` AS `friend_id`,`permission`.`id` AS `permission_id`,`permission`.`name` AS `permission_name`,`permission`.`description` AS `permission_description`,`permissionset_permission_relation`.`enabled` AS `permission_enabled`,`permissionset_permission_relation`.`disabled` AS `permission_disabled` from (((((`user_friend_relation` join `user` `user_user` on((`user_friend_relation`.`user` = `user_user`.`id`))) join `user` `user_friend` on((`user_friend_relation`.`friend` = `user_friend`.`id`))) join `permission_set` on((`user_friend_relation`.`permission_set` = `permission_set`.`id`))) join `permissionset_permission_relation` on((`permission_set`.`id` = `permissionset_permission_relation`.`permission_set`))) join `permission` on((`permissionset_permission_relation`.`permission` = `permission`.`id`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contactgroup`
--
ALTER TABLE `contactgroup`
  ADD PRIMARY KEY (`id`),
  ADD KEY `owner` (`owner`),
  ADD KEY `permission_set` (`permission_set`);

--
-- Indexes for table `guild`
--
ALTER TABLE `guild`
  ADD PRIMARY KEY (`id`),
  ADD KEY `creator` (`creator`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissionset_permission_relation`
--
ALTER TABLE `permissionset_permission_relation`
  ADD PRIMARY KEY (`permission_set`,`permission`),
  ADD KEY `permission` (`permission`);

--
-- Indexes for table `permission_set`
--
ALTER TABLE `permission_set`
  ADD PRIMARY KEY (`id`),
  ADD KEY `creator` (`creator`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`);

--
-- Indexes for table `status_detail`
--
ALTER TABLE `status_detail`
  ADD KEY `permission_id` (`permission_id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_contactgroup_relation`
--
ALTER TABLE `user_contactgroup_relation`
  ADD PRIMARY KEY (`user`,`friend`,`contactgroup`),
  ADD KEY `contactgroup` (`contactgroup`),
  ADD KEY `friend` (`friend`);

--
-- Indexes for table `user_friend_relation`
--
ALTER TABLE `user_friend_relation`
  ADD PRIMARY KEY (`user`,`friend`),
  ADD KEY `friend` (`friend`),
  ADD KEY `permission_set` (`permission_set`);

--
-- Indexes for table `user_guild_relation`
--
ALTER TABLE `user_guild_relation`
  ADD PRIMARY KEY (`user`,`guild`),
  ADD KEY `guild` (`guild`),
  ADD KEY `permission_set` (`permission_set`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contactgroup`
--
ALTER TABLE `contactgroup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `guild`
--
ALTER TABLE `guild`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `permission_set`
--
ALTER TABLE `permission_set`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `contactgroup`
--
ALTER TABLE `contactgroup`
  ADD CONSTRAINT `contactgroup_ibfk_1` FOREIGN KEY (`owner`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `contactgroup_ibfk_2` FOREIGN KEY (`permission_set`) REFERENCES `permission_set` (`id`);

--
-- Constraints for table `guild`
--
ALTER TABLE `guild`
  ADD CONSTRAINT `guild_ibfk_1` FOREIGN KEY (`creator`) REFERENCES `user` (`id`);

--
-- Constraints for table `permissionset_permission_relation`
--
ALTER TABLE `permissionset_permission_relation`
  ADD CONSTRAINT `permissionset_permission_relation_ibfk_1` FOREIGN KEY (`permission`) REFERENCES `permission` (`id`),
  ADD CONSTRAINT `permissionset_permission_relation_ibfk_2` FOREIGN KEY (`permission_set`) REFERENCES `permission_set` (`id`);

--
-- Constraints for table `permission_set`
--
ALTER TABLE `permission_set`
  ADD CONSTRAINT `permission_set_ibfk_1` FOREIGN KEY (`creator`) REFERENCES `user` (`id`);

--
-- Constraints for table `status`
--
ALTER TABLE `status`
  ADD CONSTRAINT `status_ibfk_1` FOREIGN KEY (`user`) REFERENCES `user` (`id`);

--
-- Constraints for table `status_detail`
--
ALTER TABLE `status_detail`
  ADD CONSTRAINT `status_detail_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`id`),
  ADD CONSTRAINT `status_detail_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`);

--
-- Constraints for table `user_contactgroup_relation`
--
ALTER TABLE `user_contactgroup_relation`
  ADD CONSTRAINT `user_contactgroup_relation_ibfk_1` FOREIGN KEY (`contactgroup`) REFERENCES `contactgroup` (`id`),
  ADD CONSTRAINT `user_contactgroup_relation_ibfk_2` FOREIGN KEY (`friend`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `user_contactgroup_relation_ibfk_4` FOREIGN KEY (`user`) REFERENCES `user` (`id`);

--
-- Constraints for table `user_friend_relation`
--
ALTER TABLE `user_friend_relation`
  ADD CONSTRAINT `user_friend_relation_ibfk_1` FOREIGN KEY (`friend`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `user_friend_relation_ibfk_2` FOREIGN KEY (`user`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `user_friend_relation_ibfk_3` FOREIGN KEY (`permission_set`) REFERENCES `permission_set` (`id`);

--
-- Constraints for table `user_guild_relation`
--
ALTER TABLE `user_guild_relation`
  ADD CONSTRAINT `user_guild_relation_ibfk_1` FOREIGN KEY (`guild`) REFERENCES `guild` (`id`),
  ADD CONSTRAINT `user_guild_relation_ibfk_2` FOREIGN KEY (`user`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `user_guild_relation_ibfk_3` FOREIGN KEY (`permission_set`) REFERENCES `permission_set` (`id`);